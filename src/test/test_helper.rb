require File.join(File.dirname(__FILE__), '..', 'barnintel.rb')
require 'rubygems'
require 'sinatra'
require 'rack/test'
require 'mocha/api'
require 'minitest'
require 'minitest/autorun'
require 'minitest/unit'
require 'minitest/rg'

module Rack
  module Test
    DEFAULT_HOST='localhost'
  end
end

