require 'test_helper'
require 'rack/test'
require 'pry'

def app
  BarnIntel
end

include Rack::Test::Methods
describe 'routes for barnintel' do
  describe 'basic with active session' do
    describe "POST '/'" do
      it 'should ask for login' do
        post '/'
        assert_equal('The page you are looking for was not found', last_response.body)
      end
    end
    describe "POST '/login'" do
      it 'should be successful' do
        post '/login'
        refute last_response.ok?
      end
    end

    describe "POST '/logout'" do
      it 'should be successful' do
        post '/logout'
        assert_equal('http://localhost/', last_response.location, 'Logout should redirect to default landing page')
      end
    end

    describe "POST '/turnout'" do
      it 'should redirect to login' do
        post '/turnout'
        last_response.should be_redirect
        #follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

   describe "POST '/new_user'" do
      it 'should redirect to login' do
        post '/new_user'
        assert last_response.ok?
      end
    end
  end

  describe 'management without active session' do
    describe "POST '/manage'" do
      it 'should redirect to login' do
        post '/manage'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/rations'" do
      it 'should redirect to login' do
        post '/manage/rations'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/users'" do
      it 'should redirect to login' do
        post '/manage/users'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/api/horses'" do
      it 'should redirect to login' do
        post '/manage/horses'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/api/add_horse'" do
      it 'should redirect to login' do
        post '/api/add_horse'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/delete_horse'" do
      it 'should redirect to login' do
        post '/manage/delete_horse'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/delete_user'" do
      it 'should redirect to login' do
        post '/manage/delete_user'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/update_horse'" do
      it 'should redirect to login' do
        post '/manage/update_horse'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/herds/1/remove_horse/1'" do
      it 'should redirect to login' do
        post '/manage/manage/herds/1/remove_horse/1'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/herds/add_horse'" do
      it 'should redirect to login' do
        post '/manage/manage/herds/add_horse'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end

    describe "POST '/manage/update_user'" do
      it 'should redirect to login' do
        post '/manage/update_user'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://example.org/login', last_request.url)
      end
    end
  end
end
