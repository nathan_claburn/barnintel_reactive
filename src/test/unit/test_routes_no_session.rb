require_relative '../../barnintel.rb'
require_relative '../../routes/management.rb'
require_relative '../../routes/session.rb'
require 'test_helper'
require 'rack/test'

def app
  BarnIntel
end

include Rack::Test::Methods
describe 'routes for barnintel' do
  describe 'basic without active session' do
    describe "GET '/'" do
      it 'should redirect' do
        get '/'
        assert(last_response.ok?)
        assert_match(/request access/, last_response.body)
      end
    end
    describe "GET '/login'" do
      it 'should be successful' do
        get '/login'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/', last_request.url)
        
      end
    end

    describe "GET '/logout'" do
      it 'should be successful' do
        get '/logout'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/', last_request.url)
      end
    end

    describe "GET '/confirmation'" do
      it 'should redirect to login' do
        get '/confirmation'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/', last_request.url)
      end
    end

    describe "GET '/api/chore'" do
      it 'should redirect to login' do
        get '/api/chore'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/turnout'" do
      it 'should redirect to login' do
        get '/api/turnout'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/rations'" do
      it 'should redirect to login' do
        get '/api/rations'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/horses'" do
      it 'should redirect to login' do
        get '/api/horses'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

   describe "GET '/new_user'" do
      it 'should show new user page' do
        get '/new_user'
        assert(last_response.ok?)
        assert_equal('http://localhost/new_user', last_request.url)
      end
    end
  end

  describe 'management without active session' do
    describe "GET '/api/manage'" do
      it 'should redirect to login' do
        get '/api/manage'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/rations'" do
      it 'should redirect to login' do
        get '/api/manage/rations'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/users'" do
      it 'should redirect to login' do
        get '/api/manage/users'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/horses'" do
      it 'should redirect to login' do
        get '/api/manage/horses'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/add_horse'" do
      it 'should redirect to login' do
        get '/api/manage/add_horse'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/delete_horse'" do
      it 'should redirect to login' do
        get '/manage/delete_horse'
        puts last_response.body
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_response.body)
      end
    end

    describe "GET '/api/manage/delete_user'" do
      it 'should redirect to login' do
        get '/manage/delete_user'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/update_horse'" do
      it 'should redirect to login' do
        get '/api/manage/update_horse'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/herds/1/remove_horse/1'" do
      it 'should redirect to login' do
        get '/api/manage/manage/herds/1/remove_horse/1'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/herds/add_horse'" do
      it 'should redirect to login' do
        get '/api/manage/manage/herds/add_horse'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end

    describe "GET '/api/manage/update_user'" do
      it 'should redirect to login' do
        get '/api/manage/update_user'
        #last_response.should be_redirect
        follow_redirect!
        assert_equal('http://localhost/login', last_request.url)
      end
    end
  end
end
