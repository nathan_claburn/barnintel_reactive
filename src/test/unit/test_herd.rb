require 'test_helper'
require_relative '../../lib/user'
require_relative '../../lib/herd'
require 'data_mapper'

describe Herd  do
  before(:all) do
    puts "Configuring DataMapper for test"
    DataMapper::setup(:default, 'sqlite::memory:')
    DataMapper::Model.raise_on_save_failure = true
    DataMapper.auto_migrate!
  end

  after(:each) do
  end

  describe 'create_herd' do
    it 'should create a herd from parameters' do
      herd = Herd.create(name: 'test herd', note: 'some random note')
      herd.horses.create(name: 'test horse', paddock_turnout: false)
      assert_equal('test herd', herd.name)
      assert_equal('some random note', herd.note)
    end
  end

  describe 'remove_horse' do
    it 'should remove the horse from the herd' do
      herd = Herd.create(name: 'test herd', note: 'some random note')
      horse = Horse.new(name: 'test horse', paddock_turnout: false)
      herd.horses << horse
      herd.save
      assert_equal(1, herd.horses.count)
      herd.remove_horse horse.id
      assert_equal(0, herd.horses.count)
    end
  end

  describe 'add_horse' do
    it 'should add the horse to the herd' do
      herd = Herd.create(name: 'test herd', note: 'some random note')
      horse = Horse.create(name: 'test horse', paddock_turnout: false)
      herd.add_horse(horse)
      assert_equal(1, herd.horses.count)
      
      horse = Horse.create(name: 'test horse2', paddock_turnout: false)
      herd.add_horse(horse.id)
      assert_equql(2, herd.horses.count)
      herd.add_horse(horse.id)
      assert_equal(2, herd.horses.count)
    end

    it 'should not add the horse to the herd' do
      herd = Herd.create(name: 'test herd', note: 'some random note')
      lambda { herd.add_horse(-1) }
    end
  end

  describe 'herds and horses' do
    it 'should return all horses in the herd' do
      herd = Herd.create(name: 'test herd', note: 'some random note')
      herd.add_horse(Horse.create(name: 'test horse 2', paddock_turnout: false))
      herd.add_horse(Horse.create(name: 'test horse 1', paddock_turnout: false))
      herd.add_horse(Horse.create(name: 'test horse 3', paddock_turnout: false))
      
      horses = herd.horse_list

      assert_equal('test horse 1', horses[0].name)
      assert_equal('test horse 2', horses[1].name)
      assert_equal('test horse 3', horses[2].name)
    end
  end
end

