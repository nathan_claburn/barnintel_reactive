require 'test_helper'
require_relative '../../lib/user'
require_relative '../../lib/horse'
require 'data_mapper'

describe Horse do
  before(:all) do
    puts "Configuring DataMapper for test"
    DataMapper::setup(:default, 'sqlite::memory:')
    DataMapper::Model.raise_on_save_failure = true
    DataMapper.auto_migrate!
    @herd = Herd.create(name: 'test herd')
    @herd.horses << Horse.create(name: 'test horse', paddock_turnout: false)
  end

  after(:each) do
  end

  describe 'horse creation' do
    it 'should create a horse from parameters' do
      horse = Horse.create(name: 'test horse', paddock_turnout: false)
      assert_equal('test horse' ,horse.name)
      refute(horse.paddock_turnout)
    end
  end

  describe 'horses and herds' do
    it 'should add the horse to the given herd' do
      horse = Horse.create(name: 'test horse', paddock_turnout: false)
      horse.add_to_herd @herd.id 
      h = @herd.horses.first :id => horse.id
      assert_equal('test horse', h.name)
      
      herd = Herd.create(name: 'test herd 2')
      horse.add_to_herd herd.id 
      assert_equal('test herd 2', horse.herd.name)
    end

    it 'should not add the horse to the given herd' do
      horse = Horse.create(name: 'test horse', paddock_turnout: false)
      lambda { horse.add_to_herd(-1) }
    end

  end

  describe 'horses' do
    it 'should get alphabetical listing of horses' do
      Horse.create(name: 'f', paddock_turnout: false)
      Horse.create(name: 'b', paddock_turnout: false)
      Horse.create(name: 'c', paddock_turnout: false)
      Horse.create(name: 'e', paddock_turnout: false)
      Horse.create(name: 'a', paddock_turnout: false)
      Horse.create(name: 'd', paddock_turnout: false)

      horses = Horse.alphabetical_horses

      assert_equal('a', horses[0].name)
      assert_equal('b', horses[1].name)
      assert_equal('c', horses[2].name)
      assert_equal('d', horses[3].name)
      assert_equal('e', horses[4].name)
    end
  end
end

