require 'test_helper'
require_relative '../../lib/user'
require 'data_mapper'

describe User  do
  before(:all) do
    puts "Configuring DataMapper for test"
    DataMapper::setup(:default, 'sqlite::memory:')
    DataMapper::Model.raise_on_save_failure = true
    DataMapper.auto_migrate!
  end

  after(:each) do
  end
  #before(:each) do
    #@confirmed_user = User.create(id: 1, email: :email, password: :password, nickname: :nickname, manager: false, confirmed: true)
    #@unconfirmed_user = User.create(id: 1, email: :email, password: :password, nickname: :nickname, manager: false, confirmed: false)
    #@unconfirmed_manager = User.create(id: 1, email: :email, password: :password, nickname: :nickname, manager: true, confirmed: false)
    #@confirmed_manager = User.create(id: 1, email: :email, password: :password, nickname: :nickname, manager: true, confirmed: true)
  #end

  describe 'user creation' do
    it 'should create user from email and params' do
      begin  
        User.create_user('test@test.com', 'password') 
      rescue DataMapper::SaveFailureError => e
        puts e
      end

      user = User.first(:email => 'test@test.com')
      refute_nil(user)
    end

    it 'should validate user' do
      begin  
        User.create_user('test@test.com', 'password') 
      rescue DataMapper::SaveFailureError => e
        puts e
      end

      user = User.first(:email => 'test@test.com')
      assert_equal('test@test.com', user.email)
      assert_equal(true, user.valid_password?('password'))
    end
  end
end

