require 'test_helper'
require_all 'helpers/helpers.rb'

describe 'form helper' do
 
  it 'should return true given the parameter' do
    assert(FormHelpers.get_boolean('true'))
    assert(FormHelpers.get_boolean('t'))
    assert(FormHelpers.get_boolean('yes'))
    assert(FormHelpers.get_boolean('y'))
    assert(FormHelpers.get_boolean('1'))
    assert(FormHelpers.get_boolean('on'))
  end

  it 'should return false given the parameter' do
    refute(FormHelpers.get_boolean('f'))
    refute(FormHelpers.get_boolean('false'))
    refute(FormHelpers.get_boolean('no'))
    refute(FormHelpers.get_boolean('n'))
    refute(FormHelpers.get_boolean('0'))
    refute(FormHelpers.get_boolean('off'))
  end
end
