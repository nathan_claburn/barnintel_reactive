require 'test_helper'
require_relative '../../lib/chore'
require_relative '../../lib/task'
require 'data_mapper'

describe Chore do
  before(:all) do
    puts "Configuring DataMapper for test"
    DataMapper::setup(:default, 'sqlite:memory')
    DataMapper::Model.raise_on_save_failure = true
    DataMapper.auto_migrate!
  end

  describe 'create chore' do
    it 'should create a chore from parameters' do
      chore = Chore.create(name: 'some chore name', description: 'some description')
      chore.tasks.create(name: 'some task')
      assert_equal('some chore name', chore.name)
    end

    it 'should create a chore and add a task' do
      chore = Chore.create(name: 'some chore name', description: 'some description')
      chore.tasks.create(name: 'some task')
      assert_equal('some task', chore.tasks.first.name)
    end

    it 'should add a task to a chore' do
      chore = Chore.create(name: 'some chore name', description: 'some description')
      task = Task.new(name: 'some task')
      task.save!
      chore.add_task task
      assert('some task', chore.tasks.first.name)
    end

    it 'should remove a task from a chore' do
      chore = Chore.create(name: 'some chore name', description: 'some description')
      task = Task.new(name: 'some task')
      task.save!
      chore.add_task task
      assert_equal(task.id, chore.tasks.first.id)
    end
  end
end
