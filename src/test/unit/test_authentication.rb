require 'test_helper'
require_relative '../../lib/authentication'
require_relative '../../lib/user'

describe 'Authenticate user' do
 
  it 'should authenticate the user' do
    user = User.create_user({email: 'test@test.com', password: 'password', confirmed: true})
    assert(Authentication::authenticated?({email: "test@test.com", password: "password"}))
  end

  it 'should not authenticate the user' do
    user = User.create_user(email: 'test@test.com', password: 'password', confirmed: true)
    #user.stub!(:confirmed).and_return(true)
    refute(Authentication::authenticated?({email: "test@tes.com", password: "password"}))
    refute(Authentication::authenticated?({email: "test@test.com", password: "assword"}))
  end

  #it 'should not authenticate the user' do
    #user = mock()
    #user.stub!(:confirmed).and_return(false)
    #Authentication::authenticated?(user).should be_false
  #end

  #it 'should authenticate manager' do
    #user = mock()
    #user.stub!(:confirmed).and_return(true)
    #user.stub!(:manager).and_return(true)
    #Authentication::manager?(user).should be_true
  #end

  #it 'should not authenticate manager' do
    #user = mock()
    #user.stub!(:confirmed).and_return(true)
    #user.stub!(:manager).and_return(false)
    #Authentication::manager?(user).should be_false
    #user.stub!(:confirmed).and_return(false)
    #Authentication::manager?(user).should be_false
    #user.stub!(:manager).and_return(true)
    #Authentication::manager?(user).should be_false
  #end

end

describe 'validate new user' do
  it 'should validate the new user' do
    valid_params = {email: 'test@test.com', password: 'password', password_confirmation: 'password'}
    assert(Authentication.new_user_valid(valid_params))
  end

  it 'should not validate the new user' do
    invalid_params = {email: '', password: 'password', password_confirmation: 'password'}
    assert(Authentication.new_user_valid(invalid_params)[:valid])
    
  end
end
