var React = require('react');
var Landing = require('./landing.jsx')
var RequestAccount = require('./requestAccount.jsx')
var Router = require('react-router')
    , DefaultRoute = Router.createDefaultRoute
    , RouteHandler = Router.RouteHandler
    , History = Router.HistoryLocation
    , Route = Router.Route;

var App = React.createClass({
    render: function () {
        return (
            /* jshint ignore:start */
            <div>
                <div className="navbar-top" role="navigation">
                    <div className="row container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">
                                <img src="img/tile.png" width="38" height="38" alt="Barn Intel"/>
                            </a>
                            <a id="navbarTitle" className="navbar-text" href="#">Barn Intel</a>
                        </div>
                        <a id="loginButton" href="/login" className="btn-sm btn btn-primary pull-right">Login</a>
                    </div>
                </div>
                {}
                <RouteHandler/>
            </div>
            /* jshint ignore:end */
        );
    }
});

var routes = (
    <Route name='app' path='/' handler={App}>
        <Route name='landing' handler={Landing}></Route>
        <Route name='requestAccount' handler={RequestAccount}></Route>
    </Route>
);

Router.run(routes, function(Handler){
    React.render(<Handler/>, document.getElementById('content'));
});

