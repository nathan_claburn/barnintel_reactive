var React = require('react');
var RequestAccount =  React.createClass({
    render: function () {
        return (
            /* jshint ignore:start */
            <form id="accountRequestForm" className="form">
                <div className="form-group">
                    <input id="mail" className="form-control" type="email" placeholder="email address"></input>
                    <input id="password" className="form-control" type="password" placeholder="password"></input>
                    <input id="passwordConfirmation" className="form-control" type="password" placeholder="confirm password"></input>
                    <button id="requestButton" className="btn-primary form-control">Request</button>
                </div>
            </form>
           /* jshint ignore:end */
        );
    },
    handleSubmit: function(e){
        alert("Submitted");
    }
});