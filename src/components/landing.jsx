var React = require('react');

var Landing =  React.createClass({
    render: function () {
        return (
            /* jshint ignore:start */
            <div className="row col-lg-12">
                <div className="col-md-4 thumbnail">
                    <div className="caption">
                        <h4 className="media-heading">Management</h4>
                        <p> Manage your facility remotely. Assign worker tasks, manage feed schedules, and more from your mobile device.
                            <img className="media-img img-responsive" src="img/barn.png" alt="barn picture"></img>
                        </p>
                    </div>
                </div>
                <div className="col-md-4 thumbnail">
                    <div className="caption">
                        <h4 className="media-heading">Care</h4>
                        <p>Track and update your animal's feed ration, vetting, and vaccines all from your mobile device. <img className="media-img img-responsive" src="img/feed_buckets.png" alt="feed buckets"></img>
                        </p>
                    </div>
                </div>
                <div className="col-md-4 thumbnail">
                    <div className="caption">
                        <h4 className="media-heading">Request Account</h4>
                    </div>
                </div>
            </div>
            /* jshint ignore:end */
        );
    }
});