#require_relative 'image_uploader.rb'
class Animal < Sequel::Model
  #mount_uploader :image,      AnimalUploader
  one_to_many:group, :class=>:Group, :order=>:id

  def self.get_alphabetical_animals
    Animal.all(:order => [:name.asc])
  end

  def add_to_group(group_id)
    unless Group.first(:id => group_id).nil?
      self.group_animals.destroy unless self.group_animals.nil?
      self.group = Group.first(:id => group_id)
      self.save!
    end
  end

  def self.alphabetical_animals
    Animal.all(:order => [:name.asc])
  end

  def as_json(options)
    result = super(options)
    unless(image.nil?)
      result.merge(:image_file => image.image.url)
    end
    result
  end
end

