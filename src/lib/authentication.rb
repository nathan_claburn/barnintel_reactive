module Authentication
  def self.authenticated?(params)
    user = User.first(:email => params[:Email])
    return user.valid_password?(params[:Password]) & user.confirmed unless user.nil?
    false
  end

  def self.new_user_valid(params)
    #puts params.to_s
    message =''
    valid = true
    if params[:Email].empty?
      message = "You must have a valid email"
      valid = false
    end

    if params[:Password].empty?
      message  = "You must have a password"
      valid = false
    end

    if params[:Password_confirmation].empty? or !(params[:Password].eql? params[:Password_confirmation])
      message = "Passwords do not match"
      valid = false
    end

    #puts "New user form valid? #{valid} error #{message}"
    {valid: valid, message: message}
  end
end
