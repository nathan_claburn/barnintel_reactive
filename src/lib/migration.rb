migration "create the animal table" do
  database.create_table :animal do
    primary_key :id
    text        :name
    text        :rations
    timestamp   :created_at
    timestamp   :updated_at
  end
end

migration "create the group table" do
  database.create_table :group do
    primary_key :id
    text        :name
    text        :note
    timestamp   :created_at
    timestamp   :updated_at
  end
end

migration "create the chore table" do
  database.create_table :chore do
    primary_key :id
    text        :name
    text        :description
    timestamp   :created_at
    timestamp   :updated_at
  end
end

migration "create the task table" do
  database.create_table :task do
    primary_key :id
    text        :name
    text        :description
    text        :footer
    text        :color
    timestamp   :created_at
    timestamp   :updated_at
  end
end

migration "create the user table" do
  database.create_table :user do
    primary_key :id
    text        :email
    text        :name
    text        :password
    text        :password_salt
    timestamp   :created_at
    timestamp   :updated_at
  end
end

migration "create the facility table" do
  database.create_table :facility do
    primary_key :id
    text        :name
    text        :email
    text        :number
    text        :street
    text        :city
    text        :state
    text        :zip
  end
end

migration "create the image table" do
  database.create_table :image do
    primary_key :id
  end
end
