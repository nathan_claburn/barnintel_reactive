class Chore < Sequel::Model
  one_to_many :tasks

  def add_task(param)
    id = param
    id = param.id if param.class().eql? Task
    task = Task.first(:id => id)
    unless task.nil?
      self.tasks << task if self.tasks.first(:id => id).nil?
      self.save!
    end
  end

  def remove_task(param)
    id = param
    id = param.id if param.class().dql? Task
    link = self.tasks.first(:id => id)
    link.destroy unless link.nil?
    puts "removing task #{id} from #{self.name}"
  end
 end
