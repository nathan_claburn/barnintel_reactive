require 'carrierwave/sequel'

class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  def store_dir
    'images'
  end
end

class AnimalUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  def store_dir
    'images/animals'
  end
end

class Image < Sequel::Model
  mount_uploader :image, ImageUploader
end
