#require 'json'

class BarnIntel < Sinatra::Base

  before '/api/*'  do
    puts "BEFORE API"
    content_type 'application/json'
    if session['user_id'].nil?
      flash[:error] = "You must login" if session['user_id'].nil?
      puts "REDIRECTING BECAUSE NOT LOGGED IN"
      redirect :'/login'
    end
  end

  get '/api/manage' do
    user = User.first(:id => session['user_id'])
    erb :manage 
  end

  get '/api/rations' do
    @horses = Horse.get_alphabetical_horses || []
    erb '/rations'.to_sym
  end

  get '/api/chore' do
    erb '/chore'.to_sym
  end

  get '/api/users' do
    @users = User.all
    erb :'/users'
  end

  get '/api/horses' do
    @horses = Horse.get_alphabetical_horses.to_json
  end

  post '/api/add_horse' do

    puts "PARAMS = #{params.to_s}" 
    horse = Horse.first(:name => params[:name])
    puts "HORSE = #{horse.to_s}"
    if horse.nil?
      puts "creating horse"

      unless params[:image].nil?
        puts "Storing horse image #{params[:image]}"
        upload = Image.new
        upload.image.store! params[:image]
        upload.save
        puts "Horse image #{params[:image]} saved"

      end

      horse = Horse.create(name: params['name'], rations: params['rations'], paddock_turnout: FormHelpers.get_boolean(params['paddock'].to_s), :image => upload)
      horse.add_to_herd params['select-herd']

      flash[:notice] = "Horse #{horse.name} added"
      redirect '/horses'
    else
      flash[:notice] = "Horse '#{horse.name}' already exists!"
      redirect '/add_horse'
    end
  end

  get '/api/delete_horse/:horse_id' do
    #puts "DELETING HORSE with id #{params['horse_id']}"
    horse =  Horse.first(:id => params['horse_id'])
    #puts "found horse #{horse.name}"
    flash.now[:notice] = "Horse #{horse.name} removed"
    #puts "deleting horse #{horse.name}"
    horse.destroy!
    #puts "horse #{horse.name} deleted"
    redirect '/horses'.to_sym
  end

  get '/api/delete_user/:user_id' do
    #puts "DELETING USER with id #{params['user_id']}"
    user = User.first(:id => params['user_id'])
    flash.now[:notice] = "User #{user.name} removed"
    user.destroy!
    redirect '/users'.to_sym
  end

  post '/api/update_horse' do
    #binding.pry
    horse = Horse.first(:id => params['horse_id'])
    begin
      horse.update(:name => params['name'], 
                   :rations => params['rations'], 
                   :paddock_turnout => FormHelpers.get_boolean(params['paddock']),
                   :image => params['image'])
    rescue DataMapper::SaveFailureError => e
      logger.error e.resource.errors.inspect
      redirect '/horses'.to_sym
    end
    horse.add_to_herd(params['herd_id'])
    #logger.error "Error updating horse" unless horse.save
    horse.to_json
  end

  post '/api/update_horse_image' do
    binding.pry
    horse = Horse.first(:id => params['horse_id'])
    begin
      unless params[:file].nil?
        puts "Storing horse image #{params[:file]}"
        upload = Image.new
        upload.image.store! params[:file]
        upload.save
        puts "Horse image #{params[:file]} saved"
      end
    rescue DataMapper::SaveFailureError => e
      logger.error e.resource.errors.inspect
      redirect '/horses'.to_sym
    end
    logger.info horse
    horse.to_json
  end

  post '/api/update_user' do
    #puts "Update user says session id = #{session['user_id']}"
    user = User.first(:id => params['user_id'])
    confirmed = user.confirmed
    user.confirmed = FormHelpers.get_boolean(params['confirmed'].to_s)
    user.manager = FormHelpers.get_boolean(params['manager'].to_s)
    user.save!
    if !confirmed && user.confirmed
      send_confirmation_email(user)
    end
    flash[:notice] = "#{user.name} updated"
    redirect '/users'
  end

  get '/api/herds' do
    if User.manager?(session['user_id'])
      @herds = Herd.all(:order => [:name.asc])
      return @herds.to_json(:method => :horse_list)
    else
      return "{'error': 'You do not have permission to manage herds'}"
    end
  end

  post '/api/herds/remove_horse' do
    if User.manager?(session['user_id'])
      logger.info "removing horse #{params[:horse_id]} from herds"
      Herd.all.each { |herd| herd.remove_horse params[:horse_id] }
    end
    redirect :'/horses'.to_sym
  end

  get '/api/herds/:herd_id/remove_horse/:horse_id' do
    if User.manager?(session['user_id'])
      logger.info "removing horse #{params[:horse_id]} from herd #{params[:herd_id]}"
      herd = Herd.first(:id => params[:herd_id])
      horse = Horse.first(:id => params[:horse_id])
      herd.remove_horse params[:horse_id] unless herd.nil?
      flash[:notice] = "Removed #{horse.name} from #{herd.name}"
      #erb :'/herds'.to_sym
      redirect :'/herds'.to_sym
    else
      flash[:notice] = 'You do not have permission to manage herds'
      redirect '/'
    end
  end

  post '/api/herds/add_horse' do
    logger.info "herd add horse params is #{params}"
    if User.manager?(session['user_id'])
      herd = Herd.first(:id => params[:herd_id])
      herd.add_horse params[:horse_id] unless herd.nil?
      logger.info "herd #{herd.name} adding horse #{Horse.first(:id => params[:horse_id]).name}" unless herd.nil?
    end
    herd.to_json(:only => [:name, :id], methods: [:horse_list])
  end

  post '/api/add_herd' do
    if User.manager?(session['user_id'])
      Herd.create(name: params['herd_name'], note: params['herd_note'])
    end
    redirect :'/herds'.to_sym
  end
end
