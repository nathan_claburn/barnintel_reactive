require 'pry'

class BarnIntel < Sinatra::Base

  post '/create_user' do
    user_validation = Authentication.new_user_valid(params) 
    if user_validation[:valid]
      puts "#{params.to_s}"
      user = User.create_user(params)
      #session['user_id'] = user.id
      send_new_user_notificaton(user)
      redirect 'partials/confirmation'
    else
      render 'new_user'#, :locals => params
    end
  end

  get '/new_user' do
    send_file File.join(BarnIntel.partials, '/new_user.html')
  end

  get '/login' do
    redirect '/'.to_sym
  end
  
  post '/login' do
    user = User.first(:email => params['Email'])
    if user.nil?
      puts "User with email #{params['Email']} not found"
      return { status: 'error', error: 'Email not found or password is incorrect' }.to_json
    end
    
    unless user.confirmed?
      redirect :confirmation
    end

    puts "User with email #{params['Email']} found!"
    if Authentication.authenticated?(params)
      puts "USER #{user.name} authenticated"
      session['user_id'] = user.id
    else
      puts "BAD PASSWORD FOR USER #{user.name}"
      return { status: 'error', error: 'Email not found or password is incorrect' }.to_json
    end
    return { status: 'success' }.to_json
  end

  post '/logout' do
    session['user_id'] = nil
    redirect '/'.to_sym
  end

  get '/confirmation' do
    redirect '/'.to_sym if session['user_id'].nil?
    html '/partials/confirmation'.to_sym
  end

  not_found do
    'The page you are looking for was not found'
  end
end
