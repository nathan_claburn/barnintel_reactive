require 'bcrypt'

class User < Sequel::Model

  def self.manager?(user_id)
    user = User.first(:id => user_id)
    user.nil? ? false : user.manager & user.confirmed
  end

  def self.create_user(params)
    puts params
    user = User.create(:email => params[:Email])
    if user
      puts "created user"
      user.password_salt = BCrypt::Engine.generate_salt.to_s
      user.password_hash = BCrypt::Engine.hash_secret(params[:Password], user.password_salt.to_s)
      user.confirmed = params[:confirmed] if params[:confirmed]
      user.save
      puts "saved"
    end
    user
  end

  def valid_password?(password)
    self.password_hash.eql? BCrypt::Engine.hash_secret(password, self.password_salt.to_s)
  end

  def confirmed?
    self.confirmed
  end
end
