require 'pony'

module FormHelpers
  def self.get_boolean(value)
    return false unless value

    value.match(/(true|t|yes|y|1|on)$/i) != nil
  end
end

module UserHelpers
  def current_user
    @current_user ||= User.get(session['user_id']) if session['user_id']
  end

  def send_new_user_notificaton(user)
    return
    Pony.mail({
      :to => 'hcec_support@barnintel.com',
      :from => 'hcec_support@barnintel.com',
      :subject => "new user request for #{user.nickname}",
      :body => "New user request:\nProvider: #{user.provider}\nEmail: #{user.email}\nNickname: #{user.nickname}\n",
      :via => :smtp,
      :via_options => {
        :address                => 'smtp.gmail.com',
        :port                   => '587',
        :enable_startttls_auto  => true,
        :user_name              => 'hcec_support@barnintel.com',
        :password               => ENV['BARN_INTEL_PASS'],
        :authentication         => :plain,
        :domain                 => "barnintel.com"

      }
    })
  end

  def send_confirmation_email(user)
    return
    Pony.mail({
      :to => 'hcec_support@barnintel.com',
      :from => 'hcec_support@barnintel.com',
      :subject => "#{user.nickname} confirmation",
      :body => "You have been granted access to BarnIntel for HCEC. Just follow the link and login with your email and password\n\nhttp://hcec.barnintel.com\n",
      :via => :smtp,
      :via_options => {
        :address                => 'smtp.gmail.com',
        :port                   => '587',
        :enable_startttls_auto  => true,
        :user_name              => 'hcec_support@barnintel.com',
        :password               => ENV['BARN_INTEL_PASS'],
        :authentication         => :plain,
        :domain                 => "barnintel.com"
      }
    })
  end
end

