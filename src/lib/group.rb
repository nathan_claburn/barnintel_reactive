class Group < Sequel::Model
  one_to_many :animals

  def remove_animal(id)
    link = self.group_animals.first(:animal_id => id)
    link.destroy unless link.nil?
    puts "removing animal #{id} from #{self.name}"
  end

  def add_animal(param)
    id = param
    if param.class().eql? Horse
      id = param.id
    end

    animal = Horse.first(:id => id)
    unless animal.nil?
      self.animals << animal  if self.group_animals.first(:animal_id => id).nil?
      self.save!
    end
  end

  def animal_list
    animals(:order => [:name.asc])
  end

  def as_json(options = {})
    result = super(options)
    result.merge(:animals => animals)
  end
end
