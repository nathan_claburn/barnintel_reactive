

# Warbler web application assembly configuration file
Warbler::Config.new do |config|
  config.features += ['executable']
  config.dirs = %w(app config)
  config.includes = FileList["barnintel.rb"]
  config.excludes = FileList["**/*/*.box"]
  config.bundle_without = []
  config.jar_name = "barnintel"
end
