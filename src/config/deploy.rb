require 'bundler/capistrano'
require 'rvm/capistrano'

set :application, "set your application name here"
set :repository,  "set your repository location here"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :use_sudo, false
set :scm, :git
set :repository, "git@bitbucket.org:nathan_claburn/barnintel.git"
set :user, "deploy"
set :rvm_ruby_string, '1.9.3'
set :rvm_type, :system

server "hcec.barnintel.com", :app, :webb, :db, :primary => true
set :deploy_to, "/var/www/sites/hcec.barnintel.com"

#set :stages, ['staging', 'production']
#set :default_stage, 'production'

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts
#

# If you are using Passenger mod_rails uncomment this:
 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
 end
