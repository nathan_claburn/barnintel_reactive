require 'rubygems'
require 'sinatra'
require 'sinatra/sequel'
require 'require_all'
#require 'rack/ssl-enforcer'

if RUBY_PLATFORM.eql? 'java'
  require 'jdbc/postgres'
  require 'jdbc/sqlite3'
end

DB = Sequel.connect('sqlite://test.db')

Dir["#{Dir.pwd}/lib/*.rb", "#{Dir.pwd}/lib/helpers/*.rb", "#{Dir.pwd}/lib/routes/*.rb"].each do |file|
  require file
end

class BarnIntel < Sinatra::Base
  #use Rack::SslEnforcer
  use Rack::Session::Cookie,  :key => 'hcec.barnintel.com',
                              :expire_after => 2592000,
                              :secret => ENV['SECRET_BARNINTEL']
  enable :sessions
  enable :logging
  set :session_secret,  '7106cc11-ee32-476e-80cf-ad1d2f777e25'
  helpers UserHelpers
  helpers FormHelpers

  #set :partials,        File.dirname(__FILE__) + '/app/views/partials'
  #set :views,           File.dirname(__FILE__) + '/app/views'
  set :public_folder,   File.dirname(__FILE__) + '/assets'

  configure :production do
    puts "Using production environment"
    #puts "Configuring DataMapper for development"
    #DataMapper.setup(:default, ENV['DATABASE_BARNINTEL'])
    #DataMapper.finalize.auto_upgrade!
  end

  configure :development do
    puts "Using development environment"
    #puts "Configuring DataMapper for development"
    #DataMapper.setup(:default, ENV['DEV_DATABASE_BARNINTEL'])
    #DataMapper.finalize.auto_upgrade!
  end

  configure :test do
    puts "Using test environment"
    #puts "Configuring DataMapper for test"
    #DataMapper.setup(:default, ENV['TEST_DATABASE_BARNINTEL'])
    #DataMapper.finalize.auto_upgrade!
    settings.raise_errors = true
  end

  CarrierWave.configure do |config|
    config.permissions = 0666
    config.directory_permissions = 0777
    config.storage = :file
  end

  get '/?' do
    send_file File.join(File.dirname(__FILE__) + %{/src/assets} , 'index.html')
  end
end
