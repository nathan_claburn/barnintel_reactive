var gulp = require('gulp'),
    browserify = require('browserify'),
    del = require('del'),
    source = require('vinyl-source-stream'),
    transform = require('vinyl-transform'),
    buffer = require('vinyl-buffer'),
    server = require('gulp-server-livereload'),
    watch = require('gulp-watch'),
    flatten = require('gulp-flatten'),
    reactify = require('reactify'),
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gulp-util'),
    glob = require('glob'),
    uglify = require('gulp-uglifyjs'),
    package = require('./package.json');

gulp.task('clean', function(done){
    del(['dist'], done);
});

gulp.task('browserify', function (cb) {
    var b = browserify(package.paths.app_js,
        {
            //basedir: './src/jsx',
            debug: true,
            cache: {},
            packageCache: {},
            fullPaths: true
        });
    b.transform(reactify);
    b.bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./dist'));
});

gulp.task('assets', function(){
    return gulp.src(package.paths.assets, { base: './assets/'})
        .pipe(flatten())
        .pipe(gulp.dest('./dist'));
});

gulp.task('css', function(){
    return gulp.src(package.paths.css, { base: './assets/'})
        .pipe(flatten())
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('img', function(){
    return gulp.src(package.paths.img, { base: './assets/'})
        .pipe(flatten())
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('serve', function () {
    gulp.src('./dist')
        .pipe(server({
            livereload: true,
            open: true
        }));
});

gulp.task('watch', function(){
    watch(["src/**/*.html", "src/components/*.js*", "src/**/*.rb", "src/**/*.css"], function(){
        gulp.start('default');
    });
});

gulp.task('default', ['browserify', 'assets', 'css', 'img']);
